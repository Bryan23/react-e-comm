import firebase from "firebase/app";
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyACkFQxuXht7gYoPfRt4J9g-csCUrpNUko",
    authDomain: "e-comm-db-26c42.firebaseapp.com",
    projectId: "e-comm-db-26c42",
    storageBucket: "e-comm-db-26c42.appspot.com",
    messagingSenderId: "436452794913",
    appId: "1:436452794913:web:9611fefaea3c55f4d57960",
    measurementId: "G-2BBG7KTQNT"
};

export const createUserProfileDocument = async (userAuth, additionalData) => {
    if (!userAuth) return;

    const userRef = firestore.doc(`users/${userAuth.uid}`);
    const snapShot =  await userRef.get();

    if(!snapShot.exists){
        const { displayName, email } = userAuth;
        const createdAt = new Date();

        try {
            await userRef.set({
                displayName,
                email,
                createdAt,
                ...additionalData
            })
        } catch (error) {
            console.log('error creating user', error.measure);
        }
    }

    console.log(snapShot);

    return userRef;
}

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account'});
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;