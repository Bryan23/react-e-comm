import React from 'react';
import './checkout-item.styles.scss';
import { connect } from 'react-redux';
import { clearItemFromCart, addItem, removeItem } from '../../redux/cart/cart.actions';

const CheckoutItem = ({ cartItem, clearItem, addItem, removeItem }) => {
	const { name, imageUrl, price, quantity } = cartItem;

	const removeItemFromCart = () => {
		clearItem(cartItem);
	};

	const increaseItem = () => {
		addItem(cartItem);
	};

	const decreaseItem = () => {
		removeItem(cartItem);
	};

	return (
		<div className="checkout-item">
			<div className="image-container">
				<img src={imageUrl} alt="item" />
			</div>
			<span className="name">{name}</span>
			<span className="quantity">
				<div className="arrow" onClick={decreaseItem}>
					&#10094;
				</div>
				<span className="value">{quantity}</span>
				<div className="arrow" onClick={increaseItem}>
					&#10095;
				</div>
			</span>
			<span className="price">{price}</span>
			<div className="remove-button" onClick={removeItemFromCart}>
				&#10005;
			</div>
		</div>
	);
};

const mapDispatchToProps = (dispatch) => {
	return {
		clearItem: (item) => dispatch(clearItemFromCart(item)),
		addItem: (item) => dispatch(addItem(item)),
		removeItem: (item) => dispatch(removeItem(item))
	};
};

export default connect(null, mapDispatchToProps)(CheckoutItem);
