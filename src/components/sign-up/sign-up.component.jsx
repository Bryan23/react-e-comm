import React from 'react';

import { auth, createUserProfileDocument } from '../../firebase/firebase.utils';
import CustomButton from '../custom-button/custom-button.component';
import FormInput from '../form-input/form-input.component';
import { connect } from 'react-redux';
import { setSignUpAction } from '../../redux/sign-up/sign-up.actions';

class SignUp extends React.Component {	
	handleSubmit = async (event) => {
		event.preventDefault();

		const { displayName, email, password, confirmPassword } = this.props.signUpDetails;

		if (password !== confirmPassword) {
			alert('Password not match');
			return;
		}

		try {
			const { user } = await auth.createUserWithEmailAndPassword(email, password);
			await createUserProfileDocument(user, { displayName });
			this.props.setSignUpAction({
				displayName: '',
				email: '',
				password: '',
				confirmPassword: ''
			});
		} catch (error) {
			console.log(error);
		}
	};

	handleChange = (event) => {
		const { value, name } = event.target;

		this.props.setSignUpAction({
			[name]: value
		});
	};

	render() {
		return (
			<div className="sign-up">
				<h2 className="title">I do not have an account</h2>
				<span>Sign up with your email and password</span>
				<form className="sign-up-form" onSubmit={this.handleSubmit}>
					<FormInput
						type="text"
						name="displayName"
						value={this.props.signUpDetails.displayName}
						onChange={this.handleChange}
						label="Display name"
						required
					/>

					<FormInput
						type="email"
						name="email"
						value={this.props.signUpDetails.email}
						onChange={this.handleChange}
						label="Email"
						required
					/>

					<FormInput
						type="password"
						name="password"
						value={this.props.signUpDetails.password}
						onChange={this.handleChange}
						label="Password"
						required
					/>

					<FormInput
						type="password"
						name="confirmPassword"
						value={this.props.signUpDetails.confirmPassword}
						onChange={this.handleChange}
						label="Confirm Password"
						required
					/>

					<CustomButton type="submit"> Sign Up </CustomButton>
				</form>
			</div>
		);
	}
}

const mapStateToProps = ({ signUp }) => {
	return {
		signUpDetails: signUp
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		setSignUpAction: (signUp) => dispatch(setSignUpAction(signUp))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
