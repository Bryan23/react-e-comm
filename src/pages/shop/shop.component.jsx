import React from 'react';
import CollectionPreview from './preview-collection/collection-preview.component';
import ShopData from './shop.data';

class ShopPage extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			collections: ShopData
		};
	}

	render() {
		const { collections } = this.state;
		return (
			<div className="shop-page">
				{collections.map(function({ id, ...otherCollectionProps }) {
					return <CollectionPreview key={id} {...otherCollectionProps} />;
				})}
			</div>
		);
	}
}

export default ShopPage;
