import React from 'react';
import CollectionItem from '../../../components/collection-item/collection-item.component';
import './collection-preview.styles.scss';

const CollectionPreview = ({ title, items }) => {
	return (
		<div className="collection-preview">
			<h1 className="title">{title.toUpperCase()}</h1>
			<div className="preview">
				{items
					.filter(function(_item, index) {
						return index < 4;
					})
					.map(function(item) {
						return <CollectionItem key={item.id} item={item}/>;
					})}
			</div>
		</div>
	);
};

export default CollectionPreview;
