export const UserActionTypes = {
    SET_CURRENT_USER: 'SET_CURRENT_USER',
    SET_SIGN_UP: 'SET_SIGN_UP'
};