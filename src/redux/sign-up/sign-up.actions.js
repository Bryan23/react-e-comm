import { UserActionTypes } from "../user/user.types";

export const setSignUpAction = (signUp) => {
    return {
        type: UserActionTypes.SET_SIGN_UP,
        payload: signUp
    };
};