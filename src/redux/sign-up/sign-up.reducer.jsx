import { UserActionTypes } from '../user/user.types';

const initialState = {
	displayName: '',
	email: '',
	password: '',
	confirmPassword: ''
};

const signUpReducer = (state = initialState, action) => {
	switch (action.type) {
		case UserActionTypes.SET_SIGN_UP:
			return {
				...state,
				...action.payload
			};

		default:
			return state;
	}
};

export default signUpReducer;
