import { combineReducers } from "redux";
import cartReducer from "./cart/cart.reducer";
import signUpReducer from "./sign-up/sign-up.reducer";
import userReducer from "./user/user.reducer";

export default combineReducers({
    user: userReducer,
    signUp: signUpReducer,
    cart: cartReducer
});